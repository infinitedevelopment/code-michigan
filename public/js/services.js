/**
 * API wrapper for Angular.js
 *
 * This is used to get endpoint data from a RESTful API.
 *
 * To use you need to change $resource('INSERT_YOUR_URL',{ JSON_ITEM_YOU_WANT:@JSON_ITEM_YOU_WANT }, {'query': "ARGUMENTS FOR QUERY OPTION"})
 *
 * return the apiData variable for a json output.
 *
 */

var apiConsumer = angular.module('starter.services', ['ngResource'])

/**
 * A simple example service that returns some data.
 */
apiConsumer.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var friends = [
    { id: 0, name: 'Scruff McGruff' },
    { id: 1, name: 'G.I. Joe' },
    { id: 2, name: 'Miss Frizzle' },
    { id: 3, name: 'Ash Ketchum' }
  ];

  return {
    all: function() {
      return friends;
    },
    get: function(friendId) {
      // Simple index lookup
      return friends[friendId];
    }
  }
})

/**
 * getPassportPerksData
 *
 *  To use you need to change $resource('INSERT_YOUR_URL', {JSON_ITEM_YOU_WANT:@JSON_ITEM_YOU_WANT}, {'query': "ARGUMENTS FOR QUERY OPTION"});
 *
 * return the apiData variable for a json output.
 *
 */

//create a factory and pass it the endpoint as a argument
apiConsumer.factory('getPassportPerksData', function($resource) {
    // set the endpoint variable and query to get the json
    var apiData = $resource('',
        { Resource:'@Resource' },
        { 'query':
        { isArray: true, method:'GET', responseType:'json' }
        }
    );
    //return the json data to the frontend.
    return apiData;

})

/**
 * getAPIData
 *
 *  To use you need to change $resource('INSERT_YOUR_URL', {JSON_ITEM_YOU_WANT:@JSON_ITEM_YOU_WANT}, {'query': "ARGUMENTS FOR QUERY OPTION"});
 *
 * return the apiData variable for a json output.
 *
 */

//create a factory and pass it the endpoint as a argument
apiConsumer.factory('getAPIData', function($resource) {
    //var apiData = $resource('https://resplendent-fire-7652.firebaseio.com/statuses/.json',
    // set the endpoint variable and query to get the json
    var apiData = $resource('http://data.michigan.gov/resource/ys2p-gy26.json',
        { Resource:'@Resource' },
        { 'query':
        { isArray: true, method:'GET', responseType:'json' }
        }
    );
    //return the json data to the frontend.
    return apiData;

    get = function (dataId) {
      return apiData[dataId];

    }


})

;


