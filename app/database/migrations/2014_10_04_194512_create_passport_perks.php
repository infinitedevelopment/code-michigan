<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassportPerks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('passport_perks', function($table) {
            $table->string('category');
            $table->string('phone_number');
            $table->string('website_url');
            $table->string('name');
            $table->string('address');
            $table->string('discount');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('passport_perks');
	}

}
