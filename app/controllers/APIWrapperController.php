<?php
use \Httpful\Request;
class APIWrapperController extends BaseController {
    /*
    |-------------------------------------------------
    | APIWrapperController Controller File
    |-------------------------------------------------
    */

    /**
     * getPassportPerksData function to pull in remote api endpoints
     **/
    public function getPassportPerksData(){
        $uri = 'http://data.michigan.gov/resource/ys2p-gy26.json?$limit=10000';
        $response = Request::get($uri)
            ->expectsType('json')
            ->send();
        $data = json_decode($response, true);
        foreach($data as $record) {
            $newPassportPerkEntry = new PassportPerks;
            $newPassportPerkEntry->category = $record["category"];
            $newPassportPerkEntry->phone_number = $record["phone"];
            $newPassportPerkEntry->website_url = $record["websiteurl"];
            $newPassportPerkEntry->name = $record["name"];
            $address = json_decode($record["location_1"]["human_address"], true);
            $newPassportPerkEntry->address = $address["address"];
            $newPassportPerkEntry->discount = $record["discount"];
            $newPassportPerkEntry->save();
        }
    }
}