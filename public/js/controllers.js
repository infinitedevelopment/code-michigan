var apiConsumer = angular.module('starter.controllers', ['ngResource', 'ionic', 'google-maps', 'ngSanitize'])

apiConsumer.controller('SearchCtrl', function($scope, $window, getAPIData, $sce) {
   $scope.listView = true;
   $scope.mapView = false;

    $scope.clickBiz = function() {
    
    $scope.listView = true;
    $scope.mapView = false;
    //$window.alert('List View');
    }

    $scope.clickPlaces = function(){
        $scope.listView = false;
        $scope.mapView = true;
       // $window.alert('Map View');
    }

    $scope.testSlide = function() {
        $window.alert('Test!');

    }

    $scope.callSlide = function() {
        $window.alert('Calling Now!');

    }

    $scope.webSlide = function() {
        $window.alert('Web Link!');

    }

    $scope.mapSlide = function() {
        $window.alert('Routing Directions!');

    }

    //$scope. = Friends.all();

    $scope.viewList = [];

    getAPIData.query(function(data) {
        $scope.viewList = data;
        console.log($scope.viewList);
       /* self.explicitlyTrustedHtml = $sce.trustAsHtml(
        "{"address":"6327 W Coldwater Rd","city":"Flushing","state":"MI","zip":"48433"}"
        );*/

    });


})

apiConsumer.controller('SearchDetailCtrl', function($scope, $stateParams, getAPIData, $window, $ionicModal) {
      $scope.result = {
        "category" : "Home and Garden",
        "discount" : "10% off all lawn cuts w/purchase of season",
        "location" : {"address": "6327 W Coldwater Rd", "city": "Flushing", "state": "MI", "zip":"48433"},
        "name" : "A Little Off The Top Lawn and Snow",
        "phone" : "8108674935",
        "websiteurl" : "http://www.alottlc.com"
      };

    $scope.showDiscount = function() {
        $window.alert($scope.result.discount);

    }
      
    })
    

apiConsumer.controller('MenuCtrl', function($scope, $window, $ionicSideMenuDelegate) {
    
    $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
    };

})

apiConsumer.controller('RecommendationsCtrl', function($scope, $window, $ionicSideMenuDelegate) {
    $scope.recommendations = [];

    $scope.savePerks = function(){
        $window.alert('Perks Saved!');
        $window.location='#/search.html';
    }

})

apiConsumer.controller('DiscoveryCtrl', function($scope, $window) {
    $scope.interests = {
        'travel': false,
        'sports_recreation': false,
        'food': false,
        'clothing': false
    };

    $scope.interests.travel = false;
    $scope.interests.sports_recreation = false;
    $scope.interests.foods = false;
    $scope.interests.clothing = false;

    $scope.homeView = function(){
       // $window.alert('Next View');
        $window.location='#/search.html';
    };

    $scope.yesVote = function() {
        $window.alert('Like!');
    };

    $scope.noVote = function() {
        $window.alert('No Thanks!');
    };
})

apiConsumer.controller('PreferencesCtrl', function($scope) {

})

apiConsumer.controller('FriendsCtrl', function($scope, Friends) {
      $scope.friends = Friends.all();
    })

apiConsumer.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
      $scope.friend = Friends.get($stateParams.friendId);
    })

apiConsumer.controller('AccountCtrl', function($scope) {
    })

apiConsumer.controller('getAPI', function($scope, getAPIData) {
    $scope.apiData = [];
    getAPIData.query(function(data) {
        $scope.apiData = data;
        console.log($scope.apiData);
    });
})

apiConsumer.controller('getAPIDetailCtrl', function($scope, $stateParams, getAPIDataDetail) {
    $scope.apiData_item = [];
    getAPIDataDetail.query(function(data) {
        $scope.apiData_item = data;
        console.log($scope.apiData_item);
    });
})

apiConsumer.controller('getPassportPerksData', function($scope, getAPIData) {
    $scope.apiData = [];
    getAPIData.query(function(data) {
        $scope.apiData = data;
        console.log($scope.apiData);
    });
});

function MenuCtrl($scope, $ionicSideMenuDelegate) {
  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };
}

apiConsumer.controller('MapCtrl', function($scope, getAPIData) {
    var onMarkerClicked = function (marker) {
        marker.showWindow = true;
        $scope.$apply();
        //window.alert("Marker: lat: " + marker.latitude + ", lon: " + marker.longitude + " clicked!!")
    };

    $scope.googleMap = {
        center: {
            latitude: 44.7850343,
            longitude: -86.5041838
        },
        zoom: 6,
        markers: [
            {
                id: 1,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 43.089322146000484,
                longitude: -83.80543010199966,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 2,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 42.26667935100045,
                longitude: -85.19873816099965,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels",
                }
            },
            {
                id: 3,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 46.478317191000485,
                longitude: -87.70274669299965,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 4,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 42.27474071500046,
                longitude: -83.79917321199969,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 5,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 42.643176362000474,
                longitude: -83.04584800799967,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 6,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 44.657477462000486,
                longitude: -83.34211655299964,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 7,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 42.80904342200046,
                longitude: -83.88689181699965,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 8,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 41.90402761200045,
                longitude: -84.03336619799967,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 9,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 42.6479290740005,
                longitude: -85.29065798599964,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 10,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 47.103506998000455,
                longitude: -88.67148739499964,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 11,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 42.25016600100048,
                longitude: -84.21562859999966,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 12,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 45.07736575700045,
                longitude: -83.46125660399969,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 13,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 46.478317191000485,
                longitude: -87.70274669299965,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 14,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 44.29826742000046,
                longitude: -84.72984012099965,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 15,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 45.64864376300045,
                longitude: -84.47705253699968,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 16,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 44.56392464600049,
                longitude: -83.78894476299968,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 17,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 44.656517246000476,
                longitude: -83.29447902399966,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 18,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 44.24775253700045,
                longitude: -86.32293771499968,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 19,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 42.6177178960005,
                longitude: -82.53147758299968,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            },
            {
                id: 20,
                icon: 'img/pinMarkers/blue_marker.png',
                latitude: 45.776741779000474,
                longitude: -84.72664023499965,
                name: "huhuhuh",
                showWindow: false,
                options: {
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            }
        ],
        events: {
            tilesloaded: function (map, eventName, originalEventArgs) {
            },
            click: function (mapModel, eventName, originalEventArgs) {
                // 'this' is the directive's scope
                $log.log("user defined event: " + eventName, mapModel, originalEventArgs);

                var e = originalEventArgs[0];
                var lat = e.latLng.lat(),
                    lon = e.latLng.lng();
                $scope.map.clickedMarker = {
                    id: 0,
                    title: 'You clicked here ' + 'lat: ' + lat + ' lon: ' + lon,
                    latitude: lat,
                    longitude: lon
                };
                //scope apply required because this event handler is outside of the angular domain
                $scope.$apply();
            }
        }
    };

    $scope.markersEvents = {
        click: function (gMarker, eventName, model) {
            if(model.$id){
                model = model.coords;//use scope portion then
            }
            alert("Model: event:" + eventName + " " + JSON.stringify(model));
        }
    };

    _.each($scope.googleMap.markers, function (marker) {
        marker.closeClick = function () {
            marker.showWindow = false;
            $scope.$apply();
        };
        marker.onClicked = function () {
            onMarkerClicked(marker);
        };
    });

    $scope.onMarkerClicked = onMarkerClicked;

    $scope.options = {scrollwheel: false};

    $scope.marker = {
        id:0,
        coords: {
            latitude: 43.089322146000484,
            longitude: -83.80543010199966
        },
        options: { draggable: false },
        events: {
            dragend: function (marker, eventName, args) {
                $log.log('marker dragged');
                $log.log(marker.getPosition().lat());
                $log.log(marker.getPosition().lng());
            }
        }
    };

    $scope.bizResults = [];
    getAPIData.query(function(data) {
        $scope.bizResults = data;
        //console.log($scope.bizResults);
        var myJsonString = JSON.parse(JSON.stringify($scope.bizResults));
        //var myJsonString = JSON.stringify($scope.bizResults);
        //console.log(myJsonString);
        //return myJsonString
        console.log(myJsonString);
        //$scope.newData = Object[0];
    });

})

apiConsumer.controller('InfoController', function($scope, $log, $window){
        $scope.templateValue = 'hello from the template itself';
        $scope.clickedButtonInWindow = function () {
            var msg = 'clicked a window in the template!';
            $log.info(msg);
            alert(msg);
        };
})

;
